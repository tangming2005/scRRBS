### mapping summary

```bash
find *trimmed_aligned -name "*bismark_SE_report.txt" | parallel 'cat {} | sed -n '7,11p' | cut -f2 > {/.}_mapping_summary.txt'

```

These five numbers are:
```
Sequences analysed in total:
Number of alignments with a unique best hit from the different alignments:
Mapping efficiency:
Sequences with no alignments under any condition:
Sequences did not map uniquely:
```
merge them into one file:

```bash
paste *summary.txt > mapping.txt
```

read in to R and make figures
```{r}
mapping<- read.table("/Users/mtang1/projects/scRRBS/results/mapping.txt", sep="\t", header=F, stringsAsFactors = F)

#convert the % to numeric vector
mapping[3,]<- as.numeric(sub("%", "", mapping[3,]))

colnames(mapping)<- names(total)
mapping$summary <-c("total.reads", "unique.mapped", "mapping.efficiency%", "not.mapped", "not.unique.mapped") 


mapping<- mapping %>% gather(sample, statistics, 1:12 )
mapping$statistics<- as.numeric(mapping$statistics)

ggplot(mapping, aes(x=sample, y=statistics)) + geom_bar(stat = "identity") +
        facet_wrap(~summary, scales = "free") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 
```

```
 GSC1_481  GSC1_G1 GSC2_482 GSC2_SC42 GSC2_SC44 GSC2_SC47  GSC3_a  GSC3_b  GSC3_g  GSC4_21  GSC4_22   GSC4_o
1 14940525 23456696 16108233  35918442  31411415  48866628 7226305 6178668 7694082 12245793 11205490 10285670
2  6454308 11484595  7088258  12135370   1640453   3169422  627466   99312  127140   644248   177150   518581
3    43.2%    49.0%    44.0%     33.8%      5.2%      6.5%    8.7%    1.6%    1.7%     5.3%     1.6%     5.0%
4  3945142  3401439  4080297  14044036  28359484  41934643 6382432 6022136 7477225 11248846 10896612  9359636
5  4541075  8570662  4939678   9739036   1411478   3762563  216407   57220   89717   352699   131728   407453
             summary
1        total.reads
2      unique.mapped
3 mapping.efficiency
4         not.mapped
5  not.unique.mapped

```

![](https://gitlab.com/tangming2005/scRRBS/uploads/4b257da0642fb329f86adae628fa6295/mapping_summary.png)

### CpG sites number and coverage

1 based 

```{r}
setwd("/Users/mtang1/projects/scRRBS/results/coverage/")

files<- list.files(".", pattern=".txt")

for (file in files) {
        # extract the cell line name
        cell.name<-  gsub("(.+)-(.+).txt", "\\1_\\2", file)
        print (sprintf("reading in %s peak file", file))
        print(cell.name)
        print (file)
        # assign the cell-line name as the varaible name of the GRanges
        assign(cell.name, read.table(file, header=F))
}

methy_df_list<- list(GSC1_481, GSC1_G1, GSC2_482, GSC2_SC42, GSC2_SC44, GSC2_SC47, GSC3_a, GSC3_b, GSC3_g, GSC4_21, GSC4_22, GSC4_o)


meth_list<- lapply(methy_df_list, 
       function(df) {
               names(df)<- c("chr", "start", "end", "meth_per", "meth_count", "unmeth_count")
               df$coverage<- df$meth_count + df$unmeth_count
               return (df)
       }
)

names(meth_list)<- c("GSC1_481", "GSC1_G1", "GSC2_482", "GSC2_SC42", "GSC2_SC44", "GSC2_SC47", "GSC3_a", "GSC3_b", "GSC3_g", "GSC4_21", "GSC4_22", "GSC4_o")

```

**it is recommended to have [RRBS coverage](https://genohub.com/recommended-sequencing-coverage-by-application/) of 10x**


```{r}
# How many sites for each cell
total<- sapply(meth_list, 
       function(df){ 
               filter(df, chr %in% convention_chrs) %>% nrow
               }
 )


## How many > 10 reads at each CpG
library(dplyr)
cov_greater_10 <- sapply(meth_list, 
       function(df){ 
               filter(df, coverage >= 10) %>% filter(chr %in% convention_chrs) %>% nrow
               }
 )

sites_df<- data.frame(total=total, cov_greater_10 = cov_greater_10)
sum(MspI$MspI)

sites_df$protocol<- c("TangV4", "Myers", "TangV4", "TangV4", "TangV4", "TangV4", "TangV1", "TangV1", "TangV1", 
                      "TangV2", "TangV3", "TangV1")
sites_df$cell.num<- c("hundred", "bulk", "hundred", "single", "single", "single", "single", "single", "ten", "single",
                      "single", "thousand")

sites_df$sample<- rownames(sites_df)
```
How the CpG sites table look like
```
           total cov_greater_10 protocol cell.num    sample
GSC1_481  1097119         579915   TangV4  hundred  GSC1_481
GSC1_G1   1975910        1140661    Myers     bulk   GSC1_G1
GSC2_482  1122445         593971   TangV4  hundred  GSC2_482
GSC2_SC42  580411         145619   TangV4   single GSC2_SC42
GSC2_SC44  287735           7936   TangV4   single GSC2_SC44
GSC2_SC47  378594          12806   TangV4   single GSC2_SC47
GSC3_a       4239            305   TangV1   single    GSC3_a
GSC3_b       5093            177   TangV1   single    GSC3_b
GSC3_g       5202            270   TangV1      ten    GSC3_g
GSC4_21     14815            621   TangV2   single   GSC4_21
GSC4_22     22716            524   TangV3   single   GSC4_22
GSC4_o     461868           7400   TangV1 thousand    GSC4_o
```

Tidy the data to make figure:

```{r}
library(tidyr)
sites_df<- sites_df %>% gather(category, CpG.num, 1:2)
library(ggplot2)

ggplot(sites_df, aes(x=sample, y=CpG.num, fill=category)) + geom_bar(stat="identity", position = "dodge") +
        ggtitle("CpG sites number for each sample") +
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 

```
![](https://gitlab.com/tangming2005/scRRBS/uploads/8593300fb25429fc65d04e6aacc682fa/CpG_sites.png)

### Coverage(read depth) for each CpG site for each sample

Thx `floris` to show me his way to read in the data into a long-format dataframe.

```{r}
setwd("/Users/mtang1/projects/scRRBS/results/coverage/")
library(dplyr)
library(tidyr)
library(ggplot2)

files<- list.files(".", pattern=".txt")

datlist <- lapply(files, function(f) {
        dat = data.table::fread(f)
        names(dat)<- c("chr", "start", "end", "meth_per", "meth_count", "unmeth_count")
        dat$sample = gsub(".txt", "", f)
        return(dat)
})

dat<- data.table::rbindlist(datlist)

dat$cov<- dat$meth_count + dat$unmeth_count

ggplot(dat, aes(x=sample, y=log2(cov))) + geom_boxplot() + 
        ggtitle("read coverage for each CpG sites recovered by each sample") +
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 
```

![](https://gitlab.com/tangming2005/scRRBS/uploads/84ff3823c7035ae24e7bba7da5ac52a6/coverage_all.png)

**For single cells, many sites have coverage of 1 read, as the log2 value is 0**

For CpG sites have more than 10 read coverage

```{r}
ggplot(filter(dat, cov>=10), aes(x=sample, y=log2(cov))) + geom_boxplot() + 
        ggtitle("read coverage for each CpG sites (>10 reads) recovered by each sample") +
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 
```
![](https://gitlab.com/tangming2005/scRRBS/uploads/b25f7252b66e1fd6f8b2e0799a582b75/coverage_10.png)

**After filtering the CpGs with coverage more than 10 reads, the coverage for left-over CpGs is high**


### How many MspI sites in the genome?

```{r}
### How many MspI sites in the genome?

library(BSgenome.Hsapiens.UCSC.hg19)
seqnames(BSgenome.Hsapiens.UCSC.hg19)

convention_chrs<- seqnames(BSgenome.Hsapiens.UCSC.hg19)[1:25]
hg19<- BSgenome.Hsapiens.UCSC.hg19

chr1<- hg19$chr1
##MspI cutting sites
countPattern("CCGG", chr1, max.mismatch=0)

m1 <- matchPattern("CCGG", chr1, max.mismatch=0)

MspI_counts<- numeric()
for (seqs in convention_chrs) {
        print (seqs)
        MspI_count<- countPattern("CCGG", hg19[[seqs]], max.mismatch=0)
        print (MspI_counts)
        MspI_counts<- c(MspI_counts, MspI_count)
}

MspI<- data.frame(chr=convention_chrs, MspI=MspI_counts)

MspI
```

**MspI sites number on each chromosome:**
total: `sum(MspI$MspI)`
`2297221`

```
  chr   MspI
1   chr1 194422
2   chr2 167180
3   chr3 121413
4   chr4 104714
5   chr5 111994
6   chr6 110705
7   chr7 127697
8   chr8  99168
9   chr9 103249
10 chr10 109895
11 chr11 108794
12 chr12 103703
13 chr13  57675
14 chr14  70012
15 chr15  72901
16 chr16 100891
17 chr17 112455
18 chr18  50105
19 chr19 111069
20 chr20  63820
21 chr21  31238
22 chr22  58102
23  chrX  90297
24  chrY  15699
25  chrM     23
```

CpG sites recovered for each sample:

![](https://gitlab.com/tangming2005/scRRBS/uploads/206b9b8095be19edf7f00d4afd8ccbd1/CpG_chr.png)

### bimodal distribution of methylation levels
https://gitlab.com/tangming2005/scRRBS/uploads/35bae4b067ac1164d81d09a2bc92b4fe/bimodal_hist.pdf

https://gitlab.com/tangming2005/scRRBS/uploads/aad33a39e5433457bb51a539fd521164/bimodal_density.pdf


