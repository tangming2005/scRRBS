### check bisulfite conversion rate

For any DNA methylation assays, it is good to have controls to assess the bisulfite conversion rate.
most of my samples have a spike-in lambda unmethylated DNA. I mapped them to lambda genome and [calculated the efficiency](https://gitlab.com/tangming2005/scRRBS/blob/master/quality_control.md#check-bisulfite-conversion-rate).   
I do not have spike-in for one sample. I have to check the conversion efficiency for the red unmethylated C (see below)introduced at the 3’ 
when end-repair was done. for my bismark pipeline, trim_galore will remove this unmehtylated C if there is adaptor contamination.

![](https://gitlab.com/tangming2005/scRRBS/uploads/d241f535b3138b5e696a2fe2a92b7a65/Screenshot_2016-05-23_10.14.20.png)

I will need to check C`C`G + adaptor or T`C`G + adaptor for unmethylated filled-in Cs.
and C`T`G + adaptor or T`T`G + adaptor for methylated filled-in Cs.

e.g. a full-length read:

TGGATGTTGGTTGTGGTTAGTAT`TCGAGATCGGAAG`

It stats with TGG, so it is not methylated in the genome, but check at the red part, it start with TCG, so the filed-in C are unmethylated (not converted successfully by bisulfite)

Felix Krueger, the author of `Trim_glore` sent me a script for calculating the conversion rate from raw fastq files.
see it in the `scripts` folder in the same repo.

Tommy Tang: I only saw in your script you checked T`T`G + adaptor and T`C`G+ adaptor.
Do I need to check CTG and CCG as well?

>I would say in theory yes, but since we were working in mammalian genomes where you would expect a non-CG methylation of <1% we 
just assumed that the C before the CG is always converted.

Tommy Tang: Thanks, I noticed that as well and change the length accordingly.

We are checking the bisulfite conversion rate.
Although in the human genome, non-CpGs are unmethylated, if there is a bisulfite conversion failure, they will remain as Cs, and not converted to Ts.
we will miss a lot of sequences with CCG and probably few of the CTG

>In that case you might add in the other scenarios and see whether it makes a big difference. When you look at non-CG methylation 
levels in general (such as from the summary report), do you see very high levels that are indicative of conversion problems?
Frankly we got mixed results from using this method of looking at the filled-in position. Sometimes the values were very low 
(e.g. around 0.2% for the Booth et al data), but it sometimes came back with 25% methylation at that position which was clearly 
some sort of artefact since the overall level of non-CG methylation was 1% or so. So yea I would be a little careful with the 
values you get from looking at this position. If you take more global values such as (non-CG?) methylation levels over CpG islands 
as a measure, or possibly methylation of chrMT you might get better estimates for non-conversion. Cheers, Felix

Using the script Felix sent me for the untrimmed fastq with default 5 bp match of the adaptor.

```bash
./find_RRBS_non_conversion.pl GSC1-G1_merged.fastq

23866521 sequences analysed in total

CGG at the start: 10265354	(43.0% of total sequences)
TGG at the start: 8256938	(34.6% of total sequences)

Filled-in positions
Bisulfite converted: 2055
Conversion resistant: 386
Percentage non bisulfite converted: 15.81

Average Phred score for converted positions:	35.7
Average Phred score for non-converted positions:	33.4

```
Using this script, the conversion rate is `1- 15.81% = 84.2%`

Alternatively, I checked CpG methylation for chrM. The assumption is that mitochondria DNAs are unmethylated.  
Cavet: [Mitochondrial DNA methylation as a next-generation biomarker and diagnostic tool](http://www.sciencedirect.com/science/article/pii/S1096719213002576)

Mitochondrial DNA methylation [is rare](http://www.nature.com/articles/srep23421) anyways.

```bash
cat CpG_context_GSC1-G1_merged_trimmed_bismark.txt | grep chrM | wc -l
14200

cat CpG_context_GSC1-G1_merged_trimmed_bismark.txt | grep chrM | awk '$5=="z"' | wc -l
14073       

cat CpG_context_GSC1-G1_merged_trimmed_bismark.txt | grep chrM | awk '$5=="Z"' | wc -l
127
```

In this case the conversion rate is pretty high: `14073/14200= 99%`

Another way is check CHG and CHH methylation rate. non-CpG methylation is  `~1%` as pointed out by Felix.
checking the mapping report by `bismark` for the trimmed fastqs:
```bash
cat GSC1-G1_merged_trimmed_bismark_SE_report.txt
Final Cytosine Methylation Report
=================================
Total number of C's analysed:	123553626

Total methylated C's in CpG context:	12143869
Total methylated C's in CHG context:	243763
Total methylated C's in CHH context:	424932

Total unmethylated C's in CpG context:	21240257
Total unmethylated C's in CHG context:	29692059
Total unmethylated C's in CHH context:	59808746

C methylated in CpG context:	36.4%
C methylated in CHG context:	0.8%
C methylated in CHH context:	0.7%

```

The non-CpG methylation is indeed low: 0.8% and 0.7% for CHG and CHH, respectively,
indicating good bisulfite conversion rate. Othewise, we should observe a very high
methylation rate for ChG and CHH if the bisulfite conversion failed.
