#! /bin/bash
set -e
set -u
set -o pipefail

module load bowtie1
module load samtools

fastq=$1
bismark -n 1 --bowtie1 /home/mtang1/ref_genomes/UCSC_hg19_ref/ ${fastq} -o ${fastq/.fq/_aligned}