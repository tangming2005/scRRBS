set -e
set -u
set -o pipefail

fastq=$1

mkdir ${fastq/.fastq.gz/_fastqc}
fastqc ${fastq} -o ${fastq/.fastq.gz/_fastqc}

trim_galore --rrbs --dont_gzip ${fastq}
mkdir ${fastq/.fastq.gz/_trimmed_fastqc}
fastqc ${fastq/.fastq.gz/_trimmed.fq} -o ${fastq/.fastq.gz/_trimmed_fastqc}