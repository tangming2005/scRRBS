### About the samples.

From Marcos:

>We generated two pooled RRBS libraries from 10 single-cells. It is a further modification from the previous protocol 
from which you had data; in this version we pooled the single cells after barcoding but before bisulfite-treatment and 
amplification. The two pools (GSC5S and GSC5L) differ in size so we can discover which fraction is the most contaminated by 
adapter dimers.

so, there are two pools, each pool contain 10 single cells but with different fractions.

### copy the files

```bash

rsync -av  /rsrch1/genetics/htep/hiseq/160726_SN746_0451_AC96HRACXX/Unaligned/MT-RRBS_PM343 .

#cat the data for the same cell from different lanes.
mtang1@dqsfacpriv04 MT-RRBS_PM343]$ ls -d -1 Sample* | parallel -k -j5  'find {} -name "*gz" | xargs cat > {}.fastq.gz'

```

Let's just check how many reads start with `TGG`(unmethylated) or `CGG`(methylated):

```bash
zcat Sample_GSC5L-SC6-10.fastq.gz | sed -n '2~4p' | wc -l
16983503

zcat Sample_GSC5L-SC6-10.fastq.gz | sed -n '2~4p' |  grep "^CGG" | wc -l
3310492

zcat Sample_GSC5L-SC6-10.fastq.gz | sed -n '2~4p' |  grep "^TGG" | wc -l
6370336
```

calculate for every sample

`calcultate_meth_per.sh`

```bash
#! /bin/bash

set -e
set -u
set -o pipefail

sample=$1

total=$(zcat $sample | sed -n '2~4p' | wc -l)
methyl=$(zcat $sample | sed -n '2~4p' |  grep "^CGG" | wc -l)
unmethyl=$(zcat $sample | sed -n '2~4p' |  grep "^TGG" | wc -l)

methylpercent=$(echo "${methyl}"/"${total}" | bc -l)
unmethylpercent=$(echo "${unmethyl}"/"${total}" | bc -l)

echo "total reads is ${total}"
echo "reads starts with CGG is ${methyl}"
echo "percent is ${methylpercent}"
echo "reads starts with TGG is ${unmethyl}"
echo "percent is ${unmethylpercent}"

```

```bash
find *gz | parallel -j 5 './calculate_meth_per.sh {} > {.}-TGG-CGG-count.txt'
```

### fastqc and trim low quality bases (<20) and adaptors
```bash
find *fastq.gz | parallel -j 5 './trim_fastqc.sh {}'
```

### map with bismark

each `bismark` job will use 5 CPUs, I will do with a batch of 3 (15 CPUs) to not abuse the cluster.
each job takes ~2hours to finish.

```bash
find *fq | head -3 | parallel -j15 './align_hg19.sh {}'

find *fq | sed -n '4,6 p' | parallel -j15  './align_hg19.sh {}'

find *fq | sed -n '7,9 p' | parallel -j15 './align_hg19.sh {}'

find *fq | sed -n '10,12 p' | parallel -j15 './align_hg19.sh {}'

find *fq | sed -n '13,15 p' | parallel -j15 './align_hg19.sh {}'

find *fq | sed -n '16,18 p' | parallel -j15 './align_hg19.sh {}'

find *fq | sed -n '19,20 p' | parallel -j15 './align_hg19.sh {}'

```

### checking mapping efficiency reads number (mapping summary)

```bash
find *trimmed_aligned -name "*bismark_SE_report.txt" | parallel 'cat {} | sed -n '7,11p' | cut -f2 > {/.}_mapping_summary.txt'

paste *summary.txt > mapping.txt

ls -1 *summary.txt | sed 's/_trimmed_bismark_SE_report_mapping_summary.txt//' | sed 's/Sample_//' > samplenames.txt

```

Read in to R to plot

```r
mapping<- read.table("/Volumes/users5/Tommy1/scRRBS/pooled_second_trail/MT-RRBS_PM343/mapping.txt", sep="\t", header=F, stringsAsFactors = F)

sample.names<-  read.table("/Volumes/users5/Tommy1/scRRBS/pooled_second_trail/MT-RRBS_PM343/samplenames.txt", sep="\t", header=F, stringsAsFactors = F)

colnames(mapping)<- sample.names$V1

## change % to numeric
mapping[3,]<- as.numeric(sub("%", "", mapping[3,]))

mapping$summary <-c("total.reads", "unique.mapped", "mapping.efficiency%", "not.mapped", "not.unique.mapped") 

mapping<- mapping %>% gather(sample, statistics, 1:20 )
mapping$statistics<- as.numeric(mapping$statistics)

## change the factor levels
mapping$sample<- factor(mapping$sample, levels = c("GSC5L-SC6-1",  "GSC5L-SC6-2",  "GSC5L-SC6-3",  "GSC5L-SC6-4",  "GSC5L-SC6-5",  "GSC5L-SC6-6", "GSC5L-SC6-7", "GSC5L-SC6-8",  "GSC5L-SC6-9", "GSC5L-SC6-10", "GSC5S-SC6-1", "GSC5S-SC6-2",  "GSC5S-SC6-3",  "GSC5S-SC6-4",  "GSC5S-SC6-5",  "GSC5S-SC6-6",  "GSC5S-SC6-7",  "GSC5S-SC6-8",  "GSC5S-SC6-9", "GSC5S-SC6-10"))


ggplot(mapping, aes(x=sample, y=statistics)) + geom_bar(stat = "identity") +
        facet_wrap(~summary, scales = "free") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 

```
![](https://gitlab.com/tangming2005/scRRBS/uploads/8c481ad6588afd211330f68b9fd4c13f/pool_mapping.png)

**Comparing with the mapping summary with [the first batch](https://gitlab.com/tangming2005/scRRBS/blob/master/QC_figures.md),
the mapping efficiency improved a lot especially for the GSC5L pool**.  Mapping efficiency are mostly < 10% last time.

There are a lot more unique mapped reads compared with last time as well.

### methylation extract

I turned off --comprehensive to get strand specific report of methylation Cs.

`extract_methylation_from_hg19_bam.sh`:

```bash
#! /bin/bash
set -e
set -u
set -o pipefail

bam=$1

## have to make an empty dir first
mkdir ${bam/.bam/_methylation_extract}

bismark_methylation_extractor -s --bedGraph --counts --buffer_size 5G --cytosine_report --genome_folder /home/mtang1/ref_genomes/UCSC_hg19_ref/ ${bam} -o ${bam/.bam/_methylation_extract}
```

```bash
find *trimmed_aligned -name "*bam" | parallel -j 10 ./extract_methylation_from_hg19_bam.sh {}
```
### Use [methylKit](https://github.com/al2na/methylKit) for downstream analysis

Although one can read in the `.cov` output from `bismark_methylation_extractor` to R and do exploratory analysis like I [did previously](https://gitlab.com/tangming2005/scRRBS/blob/master/QC_figures.md),
it is better to use a pre-exsiting tool to analyze the data. `methylKit` can handle bismark output files, and I will test it.

**Note that the .cov files contain methylation calls on both plus and minus strand and there is no strand information provided in that file, so my previous
analysis for CpG site coverage should be revisited !!** 

Because the `.cov` output does not hava the strand information, optionally one can import the `*.CpG_report.txt` file, which contains strand information, to `methylkit`.

from `?methRead`

>Bismark aligner can output methylation information per base in multiple formats. With pipeline='bismarkCoverage', the function reads 
bismark coverage files, which have chr,start,end, number of cytosines (methylated bases) and number of thymines (unmethylated bases) format. 
If bismark coverage files are used the function will not have the strand information,so beware of that fact. With pipeline='bismarkCytosineReport', 
the function expects cytosine report files from Bismark, which have chr,start, strand, number of cytosines (methylated bases) , number of thymines 
(unmethylated bases),context and trinucletide context format.

copy the cytosin-report to a new folder:

```bash
mkdir cytosine-report-bismark

find . -name "*CpG_report.txt" | parallel cp {} cytosine-report-bismark/
```


install package. methyl-kit is on biconductor dev branch. I will install by following [this](https://www.biostars.org/p/187599/).

```{r}
source("http://bioconductor.org/biocLite.R")
biocLite(c('IRanges', 'data.table', 'S4Vectors', 'GenomeInfoDb', 'KernSmooth', 
                 'qvalue', 'emdbook', 'Rsamtools', 'gtools', 'fastseg', 'rtracklayer', 
                 'mclust', 'R.utils', 'limma', 'Rcpp', 'Rhtslib', 'zlibbioc'))
install.packages('methylkit', repos = "http://www.bioconductor.org/packages/devel/bioc")
```

MethylKit works with bismark output `*.cov` files and `*.CpG_report.txt`. see feature request [here](https://github.com/al2na/methylKit/issues/16) and was implemented in `v0.9.6`.

```{r}
library(methylKit)

setwd("/Volumes/users5/Tommy1/scRRBS/pooled_second_trail/MT-RRBS_PM343/cytosine-report-bismark/")

file.list<- as.list(list.files())
sample.name<- gsub("Sample_(.+)_trimmed_bismark.CpG_report.txt", "\\1", file.list)

scRRBS.pool<- methRead(file.list, sample.id= as.list(sample.name), assembly="hg19", pipeline= "bismarkCytosineReport", dbtype="tabix", treatment = c(rep(1, 10), rep(0,10)))
```
