### background and reading
Before I carried out any analysis, I read the following:

[Reduced Representation Bisulfite-Seq –
A Brief Guide to RRBS](http://www.bioinformatics.babraham.ac.uk/projects/bismark/RRBS_Guide.pdf)  
[Taking appropriate QC measures for
RRBS-type or other -Seq applications
with Trim Galore!](http://www.bioinformatics.babraham.ac.uk/projects/trim_galore/trim_galore_User_Guide_v0.4.1.pdf)  
[Quality Control, trimming and alignment of Bisulfite-Seq data (Prot 57)](http://www.epigenesys.eu/en/protocols/bio-informatics/483-quality-control-trimming-and-alignment-of-bisulfite-seq-data-prot-57)  
[Bisulfite-Sequencing Theoryand Quality Control](http://www.bioinformatics.babraham.ac.uk/training/Methylation_Course/BS-Seq%20theory%20and%20QC%20lecture.pdf)  

The above materials are prepared by `Felix Krueger` and `Simon R Andrews`  from Babraham Insititute where `fastqc`, `Trim Galore`, and `bismark` were developed.

Further reading:  

[Exploring genome wide bisulfite sequencing for DNA methylation analysis in livestock: a technical assessment](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4026711/)  

[Genome-scale DNA methylome and transcriptome profiling of human neutrophils](http://www.nature.com/articles/sdata201619)

[Technical Considerations for Reduced Representation Bisulfite Sequencing with Multiplexed Libraries](http://www.hindawi.com/journals/bmri/2012/741542/)

### Bismark analysis pipeline
####unzip and merge the fastq

Marcos sent me a file on 05/18/2016 inidcating the replicates and methods used:

| GSC-1         | Type of library | Protocol version | Number of cells                                                                |
|---------------|-----------------|------------------|--------------------------------------------------------------------------------|
| 42            | RRBS            | Tang V4          | single cell; technical replicate is GSC-2-42. Merge for analysis.              |
| 44            | RRBS            | Tang V4          | single cell; technical replicate is GSC-2-44. Merge for analysis.              |
| 47            | RRBS            | Tang V4          | single cell; technical replicate is GSC-2-47. Merge for analysis.              |
| G1            | RRBS            | Myers standard   | Standard RRBS from 500ng; technical replicate is GSC-2-G1. Merge for analysis. |
| 481           | RRBS            | Tang V4          | 100 cells; no technical replicate.                                             |
|               |                 |                  |                                                                                |
| GSC-2         |                 |                  |                                                                                |
| 42            | RRBS            | Tang V4          | single cell; technical replicate is GSC-1-42. Merge for analysis.              |
| 44            | RRBS            | Tang V4          | single cell; technical replicate is GSC-1-44. Merge for analysis.              |
| 47            | RRBS            | Tang V4          | single cell; technical replicate is GSC-1-47. Merge for analysis.              |
| G1            | RRBS            | Myers standard   | Standard RRBS from 500ng; technical replicate is GSC-1-G1. Merge for analysis. |
| 48 second pcr | RRBS            | Tang V4          | 100 cells; no technical replicate.                                             |
|               |                 |                  |                                                                                |
| GSC-3         |                 |                  |                                                                                |
| a             | RRBS            | Tang V1          | single cell; no replicate.                                                     |
| b             | RRBS            | Tang V1          | single cell; no replicate.                                                     |
| g             | RRBS            | Tang V1          | 10 celsl; no replicate.                                                        |
|               |                 |                  |                                                                                |
| GSC-4         |                 |                  |                                                                                |
| 21            | RRBS            | Tang V2          | single cell; no replicate.                                                     |
| 22            | RRBS            | Tang V3          | single cell; no replicate.                                                     |
| o             | RRBS            | Tang V1          | 1000 cells; no replicate                                                       |
|               |                 |                  |                                                                                |
| GSC-5         |                 |                  |                                                                                |
| e             | DREAM           | Tang DREAMV1     | single cell; no replicate.                                                     |
| f             | DREAM           | Tang DREAMV1     | single cell; no replicate.                                                     |
| p             | DREAM           | Tang DREAMV1     | 1000 cells; no replicate                                                       |

### unzip and merge the fastqs(need a better naming scheme to better stream the analysis)

The fastq files are of 36-bp read length (single end). long reads actually is [bad for RRBS](http://www.bioinformatics.babraham.ac.uk/projects/bismark/RRBS_Guide.pdf), as some of the MspI fragment are very small (< 40bp), long reads will have a lot of bases read into the adaptor regions. paired-end sequencing is not necessary better as well. In a word, 40-50bp single-end reads are good enough for RRBS.
This [protocol](http://www.nature.com/articles/sdata201619) trimmed 25bp off from the 3' of the 100bp reads.

The adaptor `AGATCGGAAGAGC` 
is frequently found in many reads. (expected for scRRBS).

**Theoretically, for directional RRBS library, all(most)reads start  with CGG or TGG, but in a real experiment we see reads start from 3 different bases (although should be few). That's because of [star activity](https://www.neb.com/tools-and-resources/usage-guidelines/star-activity) (MspI or whatever else is used cutting outside its normal site) or the general degradation caused by bisulfite treatment.**

```bash
gunzip -c GSC1-G1_S33_L004_R1_001.fastq.gz GSC2-G1_S38_L005_R1_001.fastq.gz > GSC1-G1_merged.fastq

gunzip -c GSC1-SC42_S30_L004_R1_001.fastq.gz GSC2-SC42_S35_L005_R1_001.fastq.gz > GSC2-SC42_merged.fastq

gunzip -c GSC1-SC44_S31_L004_R1_001.fastq.gz GSC2-SC44_S36_L005_R1_001.fastq.gz > GSC2-SC44_merged.fastq

gunzip -c GSC1-SC47_S32_L004_R1_001.fastq.gz GSC2-SC47_S37_L005_R1_001.fastq.gz > GSC2-SC47_merged.fastq
```

Let's just check how many reads start with `TGG`(unmethylated) or `CGG`(methylated):

```bash
cat GSC1-G1_merged.fastq | sed -n '2~4p' | wc -l
23866521

cat GSC1-G1_merged.fastq | sed -n '2~4p' | grep "^CGG" | wc -l
10265354

cat GSC1-G1_merged.fastq | sed -n '2~4p' | grep "^TGG" | wc -l
8256938
```
`GSC1-G1` is prepared by Standard RRBS from 500ng (not single cell).
the rate is pretty high: `(10265354 + 8256938)/23866521 =0.776`

Take a look of a single cell:

```bash
cat GSC2-SC42_merged.fastq | sed -n '2~4p' | wc -l
72167799

cat GSC2-SC42_merged.fastq | sed -n '2~4p' | grep "^CGG" | wc -l
10369387

cat GSC2-SC42_merged.fastq | sed -n '2~4p' | grep "^TGG" | wc -l
11415217
```
the ratio of reads start with `CGG` or `TGG` are much samller `(10369387 + 11415217)/72167799=0.30`   . This is expected. single-cell RRBS is more challenging.


#### quality trimming and adaptor trimming
**determin what [phred score](http://wiki.bits.vib.be/index.php/Identify_the_Phred_scale_of_quality_scores_used_in_fastQ) is used for the fastqs**:


```bash
wget --no-check-certificate https://raw.githubusercontent.com/brentp/bio-playground/master/reads-utils/guess-encoding.py

chmod u+x guess-encoding.py

cat GSC1-G1_merged.fastq | awk 'NR % 4 == 0' | head -n 1000000 | python ./guess-encoding.py
# reading qualities from stdin
Sanger  (43, 74)

```
trim the low-quality bases from `3'` of the reads(`phred score <20`)
and remove the adaptors, reads length < 20 bp (too short reads will have muliple mapping regions in the genome) will be discarded as well.

`trim_galore` uses `--phred33` (Sanger/illumina 1.9 +) as default: 

```bash
time trim_galore --rrbs GSC2-SC44_merged.fastq

## has to make an empty folder first
mkdir GSC1-G1_merged_trimmed_fastqc
fastqc GSC1-G1_merged_trimmed.fq -o GSC1-G1_merged_trimmed_fastqc
```

**for scRRBS, more than `50%` of the reads contain adaptors (13bp), after quality trimming `3'` low quality bases and adaptor removing, and by adding `--rrbs`, addtional two bases from 3'end will be removed to avoid the filled-in cytosine postion,  many of the reads become < 20 bp and are discarded!!**

batch process in a `for loop`: 

```bash
set -e
set -u
set -o pipefail

for fastq in *fastq
do
	## fastqc before trimming
	mkdir ${fastq/.fastq/_fastq}
	fastqc ${fastq} -o ${fastq/.fastq/_fastqc}
	
	## trimming 
	trim_galore --rrbs ${fastq}
	mkdir ${fastq/.fastq/_trimmed_fastqc}
	## fastqc again after trimming
	fastqc ${fastq/.fastq/_trimmed.fq} -o ${fastq/.fastq/_trimmed_fastqc}
done

```

parallel processing to speedup:

```bash
set -e
set -u
set -o pipefail

fastq=$1

mkdir ${fastq/.fastq.gz/_fastqc}
fastqc ${fastq} -o ${fastq/.fastq.gz/_fastqc}

trim_galore --rrbs --dont_gzip ${fastq}
mkdir ${fastq/.fastq.gz/_trimmed_fastqc}
fastqc ${fastq/.fastq.gz/_trimmed.fq} -o ${fastq/.fastq.gz/_trimmed_fastqc}
```
save it as `trim_fastqc.sh`, run it on `verhaak server`:

`find *fastq.gz | parallel -j 5 './trim_fastqc.sh {}'`

**I will need to make a `snakemake` pipeline for it on nautilus HPC**

### check bisulfite conversion rate

The phage lambda genome was downloaded from [NCBI](http://www.ncbi.nlm.nih.gov/nuccore/J02459), thx Marcos for pointing the link.

>All data are RRBS except GSC5-e, f and p. All RRBS samples are spiked in with unmethylated lambda DNA (around 0.5% of the library), that is a control for the bisulfite reaction (all cytosines should be converted to T).

**align the fastqs to phage lambda genome:**


```bash

bismark_genome_preparation --bowtie1 phage_lambda/
```

```bash
#! /bin/bash
set -e
set -u
set -o pipefail

fq=$1

bismark -n 1 --bowtie1 /home/mtang1/ref_genomes/phage_lambda ${fq} -o ${fq/.fq/_phage_aligned}

```

`find *fq | parallel -j 20 ./align_phage.sh {}`

phage lambda genome is very small, alignmen only takes ~20 mins. 


**extract methylation calls from phage lambda aligned bams (change the geome folder only)**


```bash
#! /bin/bash
set -e
set -u
set -o pipefail

bam=$1

## have to make an empty dir first
mkdir ${bam/.bam/_phage_methylation_extract}

bismark_methylation_extractor -s --bedGraph --counts --comprehensive --buffer_size 2G --cytosine_report --genome_folder /home/mtang1/ref_genomes/phage_lambda/ ${bam} -o ${bam/.bam/_phage_methylation_extract}
```
save it as `extract_methylation_from_phage_bam.sh`, 

```bash
find  *phage_aligned -name "*bam" | parallel -j 20 ./extract_methylation_from_phage_bam.sh {}
```

### bisulfite conversion rate

```bash
find *phage_aligned -name "*bismark_splitting_report.txt" | parallel 'echo {/.} | sed 's/_trimmed_bismark_splitting_report//'; cat {} | grep "C methylated in CpG context"' | paste - -

GSC1-481_S34_L004_R1_001        C methylated in CpG context:    10.2%
GSC1-G1_merged  C methylated in CpG context:    24.2%
GSC2-482_S39_L005_R1_001        C methylated in CpG context:    9.2%
GSC2-SC42_merged        C methylated in CpG context:    3.8%
GSC3-a_S40_L006_R1_001  C methylated in CpG context:    2.3%
GSC2-SC44_merged        C methylated in CpG context:    2.7%
GSC3-b_S41_L006_R1_001  C methylated in CpG context:    1.8%
GSC2-SC47_merged        C methylated in CpG context:    1.9%
GSC4-21_S43_L007_R1_001 C methylated in CpG context:    3.8%
GSC3-g_S42_L006_R1_001  C methylated in CpG context:    1.7%
GSC4-22_S44_L007_R1_001 C methylated in CpG context:    3.3%
GSC4-o_S45_L007_R1_001  C methylated in CpG context:    3.9%
```

**lambda DNA used in the spike-in is unmethylated. that means all Cs in
the lambda DNA should be converted to Ts if the bisufilte conversion is efficient. The `conversion efficiency= (1- C methylated in CpG context)`, so the the coversion efficiencies of the single cells are above 96%.** 

**Note that conversion efficency for 100 cells GSC1-481/482 are around 90% and the rate for sample prepared by standard 500ng RRBS (G1) protocal is the lowest 76%**

**update 06/07/2016. It turned out that the GSC1-G1 does not have a spike-in lambda, so the methylation rate 24.2% is not trustable. I did [something else](https://gitlab.com/tangming2005/scRRBS/blob/master/check_bisulfite_conversion_rate.md) 
to test the conversion rate for this sample**


[compare with the C methylation rate in the human GSC DNA]

### Bismark alignment to human genome
#### prepare for the reference genome

the whole genome fasta (hg19) file was gotten by:

```bash
rsync -avzP rsync://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/ .

cat *fa.gz > UCSC_hg19_genome.fa.gz
```

bisulfite convert in-silico for the reference genome:

```bash
## bowtie1 should be in PATH
module load bowtie1
mkdir UCSC_hg19_ref
mv UCSC_hg19_genome.fa UCSC_hg19_ref/

## default is bowtie2, turn on bowtie1
## bowtie1 is good for short reads and faster, but can not handel indels well.
bismark_genome_preparation --bowtie1 UCSC_hg19_ref/

```
It takes hours to prepare the genome, but this is only need to be done once.

#### alignment

for single-end:
I am not using the mulit-thread options, by its own, bismark already uses
5 threads, this can be resource hungry. If I set `mulit-thread =4`, it will use actually 20 CPUs.

have to specify `--bowtie1`, default is `bowtie2`:

```bash
bismark -n 1 --bowtie1  /home/mtang1/ref_genomes/UCSC_hg19_ref/ GSC1-G1_merged_trimmed.fq -o GSC1-G1_merged_trimmed_
aligned

```
takes roughly ~1hour for mapping.


```bash
#! /bin/bash
set -e
set -u
set -o pipefail

module load bowtie1
module load samtools

fastq=$1
bismark -n 1 --bowtie1 /home/mtang1/ref_genomes/UCSC_hg19_ref/ ${fastq} -o ${fastq/.fq/_aligned}

```
save it as `align_hg19.sh`.

only 24 cpus are avaiable on verhaak server:

```
nproc
24
```
each `bismark` job will use 5 CPUs, I will do with a batch of 3 (15 CPUs) to not 
abuse the cluster.

GSC1-G1 was done.

```bash
find *fq | grep -v "GSC1-G1" | head -3 | parallel -j15 './align_hg19.sh {}'
find *fq | grep -v "GSC1-G1" | sed -n '4,6 p' | parallel -j15  './align_hg19.sh {}'

find *fq | grep -v "GSC1-G1" | sed -n '7,9 p' | parallel -j15 './align_hg19.sh {}'

find *fq | grep -v "GSC1-G1" | sed -n '10,11 p' | parallel -j15 './align_hg19.sh {}'

```

#### alignment report

>Cytosine DNA methylation is a heritable epigenetic mark present in many eukaryotic organisms. is performed by DNA-methyltransferases that catalyze transfer of a methyl group from S-adenosyl-l-methionine to cytosine bases in DNA.
Most of mammalian and plant DNA methylation is restricted to symmetrical CG sequences, but plants also have significant levels of cytosine methylation in the symmetric context **CHG (where H is A, C or T)** and even in asymmetric sequences. 
Methylation in symmetrical sequences is preserved though cycles of DNA replication by maintenance DNA methyltransferases, which show a preference for hemimethylated substrates and methylate cytosines in the newly synthesized strand. Maintenance mechanisms for asymmetric methylation patterns are unknown, but they must include de novo methylation after each cell division.

```bash
find *trimmed_aligned -name "*SE_report.txt" | parallel -k 'echo {/.} | sed 's/_trimmed_bismark_SE_report//'; cat {} | grep "Mapp
ing efficiency"' | paste - -
GSC4-21_S43_L007_R1_001 Mapping efficiency:     5.3%
GSC2-482_S39_L005_R1_001        Mapping efficiency:     44.0%
GSC1-481_S34_L004_R1_001        Mapping efficiency:     43.2%
GSC1-G1_merged  Mapping efficiency:     49.0%
GSC3-a_S40_L006_R1_001  Mapping efficiency:     8.7%
GSC2-SC42_merged        Mapping efficiency:     33.8%
GSC4-22_S44_L007_R1_001 Mapping efficiency:     1.6%
GSC4-o_S45_L007_R1_001  Mapping efficiency:     5.0%
GSC2-SC47_merged        Mapping efficiency:     6.5%
GSC3-b_S41_L006_R1_001  Mapping efficiency:     1.6%
GSC3-g_S42_L006_R1_001  Mapping efficiency:     1.7%
GSC2-SC44_merged        Mapping efficiency:     5.2%
```

#### extract methylation calls


>if strand-specific methylation is not of interest, all available methylation information can be pooled
into a single context-dependent file (information from any of the four strands will be pooled). This will
default to three output files (CpG-context, CHG-context and CHH-context) by setting `--comprehensive`.

The `.cov` file will contain info for the plus and minus strand. So, when visualizing in a browser, the 
base could be a C or a G (C in the minus strand)

single file: 

```bash

bismark_methylation_extractor -s --bedGraph --counts --comprehensive --buffer_size 10G --cytosine_report --genome_folder /home/mtang1/ref_genomes/UCSC_hg19_ref/ GSC1-G1_merged_trimmed_bismark.bam

bismark_methylation_extractor -s --bedGraph --counts --comprehensive --buffer_size 10G --cytosine_report --genome_folder /home/mtang1/ref_genomes/phage_lambda/ GSC1-G1_merged_trimmed_bismark.bam
```
batch process and specify output directory:

```bash
#! /bin/bash
set -e
set -u
set -o pipefail

bam=$1

## have to make an empty dir first
mkdir ${bam/.bam/_methylation_extract}

bismark_methylation_extractor -s --bedGraph --counts --comprehensive --buffer_size 5G --cytosine_report --genome_folder /home/mtang1/ref_genomes/UCSC_hg19_ref/ ${bam} -o ${bam/.bam/_methylation_extract}
```
save it as `extract_methylation_from_hg19_bam.sh`. I reserved `5G` for sorting, so I restricted only 10 jobs to run concurrently for parallel.

```bash
find *trimmed_aligned -name "*bam" | parallel -j 10 ./extract_methylation_from_hg19_bam.sh {}
```

**methylation rate for the human GSCs:**

```bash
find *trimmed_aligned -name "*bismark_splitting_report.txt" | parallel 'echo {/.} | sed 's/_trimmed_bismark_splitting_report//';
 cat {} | grep "C methylated in CpG context"' | paste - -
 
GSC1-481_S34_L004_R1_001        C methylated in CpG context:    28.4%
GSC1-G1_merged  C methylated in CpG context:    36.4%
GSC2-482_S39_L005_R1_001        C methylated in CpG context:    27.3%
GSC2-SC42_merged        C methylated in CpG context:    27.4%
GSC2-SC44_merged        C methylated in CpG context:    19.5%
GSC2-SC47_merged        C methylated in CpG context:    29.4%
GSC3-a_S40_L006_R1_001  C methylated in CpG context:    75.4%
GSC4-21_S43_L007_R1_001 C methylated in CpG context:    57.1%
GSC3-g_S42_L006_R1_001  C methylated in CpG context:    30.2%
GSC4-22_S44_L007_R1_001 C methylated in CpG context:    41.9%
GSC3-b_S41_L006_R1_001  C methylated in CpG context:    32.2%
GSC4-o_S45_L007_R1_001  C methylated in CpG context:    30.6%
```

### Downstream anaysis (differential methylation regions DMR detection)

[M3D: Statistical Testing for RRBS Data Sets](https://www.bioconductor.org/packages/release/bioc/vignettes/M3D/inst/doc/M3D_vignette.pdf)  

[methylkit](https://github.com/al2na/methylKit) deals with RRBS  data.

[BiSeq](https://www.bioconductor.org/packages/release/bioc/html/BiSeq.html) RRBS centric.

revisit an edx course I have taken **HarvardX: PH525.8x Case study: DNA methylation data analysis**



### TO DO
Alternative pipelines:

[bwa-meth](https://github.com/brentp/bwa-meth/)  followed by [PileOMeth](https://github.com/dpryan79/PileOMeth)  

 I may try it. It was developed by `Brent Pedersen` now in [`Aaron Quinlan`](http://quinlanlab.org/)'s lab. A lot of good tools are from Qunlan's lab including my all-time favorite `bedtools`, `GEMINI`, `vcfanno` and `Lumpy` etc.
 
Read the [preprint: Fast and accurate alignment of long bisulfite-seq reads](http://arxiv.org/abs/1401.1129). It is for long reads. Given that we are using 36bp short reads,the results may be different as tested in the paper. I will need to compare `bwa-mem` with `bismark` for short reads.