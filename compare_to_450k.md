### compare GSC20 standard RRBS with 450k data. They should have high correlation.

install 450k annotation package 

```{r}
#source("https://bioconductor.org/biocLite.R")
#biocLite("FDb.InfiniumMethylation.hg19")
library(FDb.InfiniumMethylation.hg19)
hm450.hg19 <- getPlatform(platform='HM450', genome='hg19')
show(hm450.hg19)

## or simply
probe.450k<- get450k()

```

CpG island buit in the package. One can also download it from UCSC table browser

```{r}
data(hg19.islands)
split(hg19.islands, seqnames(hg19.islands))
```


It is a bit confusing for the 0 based or 1 based output by bismark.
the `.cov` files should be 1 based because they have the same start and end.


```{r}
scRRBS.GR<- makeGRangesFromDataFrame(dat)
scRRBS.GR<- resize(scRRBS.GR, width = 2, fix = "start")
scRRBS.GR$meth_per<- dat$meth_per
scRRBS.GR$meth_count<- dat$meth_count
scRRBS.GR$unmeth_count<- dat$unmeth_count
scRRBS.GR$sample<- dat$sample
scRRBS.GR$cov<- dat$cov

unique(scRRBS.GR$sample)

GSC1.G1.GR<- scRRBS.GR[scRRBS.GR$sample=="GSC1-G1"]

probe.RRBS.hits<- findOverlaps(probe.450k, GSC1.G1.GR, type="equal")

queryHits(probe.RRBS.hits)

GSC1.G1.overlap.450k<- GSC1.G1.GR[subjectHits(probe.RRBS.hits)]

GSC1.G1.overlap.450k$probe.name<- names(probe.450k[queryHits(probe.RRBS.hits)])

```

read in the 450k data for two cell lines.
Qianghu sent me the beta-values of GSC20 and GSC231.

only `51844` CpG sites recovered by RRBS overlaps with 450k probes.

```{r}
HM450k.data<- read.table("/Users/mtang1/projects/scRRBS/data/methy_2lines.txt", sep="\t", header=T, stringsAsFactors = F)

HM450k.data$probe.name<- rownames(HM450k.data)

GSC1.G1.overlap.450k.df <- as.data.frame(GSC1.G1.overlap.450k)

## do a left join

GSC1.G1.450k<- inner_join(GSC1.G1.overlap.450k.df, HM450k.data)

GSC1.G1.450k[1:5, 1:13]

```

Make figures to see the correlation

```{r}
ggplot(GSC1.G1.450k, aes(x=meth_per/100, y=PC20)) + geom_point()

ggplot(GSC1.G1.450k %>% filter(cov >=10), aes(x=meth_per/100, y=PC20)) + geom_point()
ggplot(GSC1.G1.450k %>% filter(cov >=10), aes(x=meth_per/100, y=PC231)) + geom_point()

GSC1.G1.450k.cov10<- GSC1.G1.450k %>% filter(cov >=10)

> cor(GSC1.G1.450k$meth_per/100, GSC1.G1.450k$PC20)
[1] 0.6955883
> cor(GSC1.G1.450k.cov10$meth_per/100, GSC1.G1.450k.cov10$PC20)
[1] 0.7441324
> cor(GSC1.G1.450k$meth_per/100, GSC1.G1.450k$PC231)
[1] 0.761661
> cor(GSC1.G1.450k.cov10$meth_per/100, GSC1.G1.450k.cov10$PC231)
[1] 0.8134395
```

**GSC1.G1.RRBS beta values versus GSC20 450k data**
![](https://gitlab.com/tangming2005/scRRBS/uploads/7fbf69d2381c2affea2d5de5e33a07c4/1.png)

**GSC1.G1.RRBS beta values versus GSC231 data**
![](https://gitlab.com/tangming2005/scRRBS/uploads/f9b8fac9d604e1fd09a226f16d9dc546/2.png)

### pair-wise correlation among different single cells.

```{r}
for (file in files) {
       # extract the cell line name
        cell.name<-  gsub("(.+)-(.+).txt", "\\1_\\2", file)
        print (sprintf("reading in %s peak file", file))
        print(cell.name)
        print (file)
        # assign the cell-line name as the varaible name of the GRanges
        assign(cell.name, read.table(file, header=F))
}

methy_df_list<- list(GSC1_481, GSC1_G1, GSC2_482, GSC2_SC42, GSC2_SC44, GSC2_SC47, GSC3_a, GSC3_b, GSC3_g, GSC4_21, GSC4_22, GSC4_o)


meth_list<- lapply(methy_df_list, 
       function(df) {
               names(df)<- c("chr", "start", "end", "meth_per", "meth_count", "unmeth_count")
               df$coverage<- df$meth_count + df$unmeth_count
               df<- df %>% filter(chr %in% convention_chrs)
               df$chr<- factor(df$chr, levels=c(paste0("chr",1:22), paste0("chr", c("X", "Y", "M"))))
               return (df)
       }
)

names(meth_list)<- c("GSC1_481", "GSC1_G1", "GSC2_482", "GSC2_SC42", "GSC2_SC44", "GSC2_SC47", "GSC3_a", "GSC3_b", "GSC3_g", "GSC4_21", "GSC4_22", "GSC4_o")

## scatter plot

G1_vs_GSC1_481<- inner_join(meth_list$GSC1_G1, meth_list$GSC1_481, by=c("chr"="chr", "start"="start", "end"="end")) %>% 
        filter(coverage.x >=10, coverage.y >=10)
cor(G1_vs_GSC1_481$meth_per.x, G1_vs_GSC1_481$meth_per.y)
ggplot(G1_vs_GSC1_481, aes(x=meth_per.x, y=meth_per.y)) + geom_point() 
 

G1_vs_GSC1_482  <-inner_join(meth_list$GSC1_G1, meth_list$GSC2_482, by=c("chr"="chr", "start"="start", "end"="end")) %>% filter(coverage.x >=10, coverage.y >=10)
cor(G1_vs_GSC1_482$meth_per.x, G1_vs_GSC1_482$meth_per.y)
ggplot(G1_vs_GSC1_482, aes(x=meth_per.x, y=meth_per.y)) + geom_point()


GSC1_481_vs_GSC2_482<- inner_join(meth_list$GSC1_481, meth_list$GSC2_482, by=c("chr"="chr", "start"="start", "end"="end")) %>% filter(coverage.x >=10, coverage.y >=10)
ggplot(GSC1_481_vs_GSC2_482, aes(x=meth_per.x, y=meth_per.y)) + geom_point() 
cor(GSC1_481_vs_GSC2_482$meth_per.x, GSC1_481_vs_GSC2_482$meth_per.y)


GSC1_G1_vs_GSC2_SC42<- inner_join(meth_list$GSC1_G1, meth_list$GSC2_SC42, by=c("chr"="chr", "start"="start", "end"="end")) %>% filter(coverage.x >=10, coverage.y >=10)
cor(GSC1_G1_vs_GSC2_SC42$meth_per.x, GSC1_G1_vs_GSC2_SC42$meth_per.y)
ggplot(GSC1_G1_vs_GSC2_SC42, aes(x=meth_per.x, y=meth_per.y)) + geom_point() 


GSC2_SC42_vs_GSC2_SC44<- inner_join(meth_list$GSC2_SC42, meth_list$GSC2_SC44, by=c("chr"="chr", "start"="start", "end"="end")) %>% filter(coverage.x >=10, coverage.y >=10)
cor(GSC2_SC42_vs_GSC2_SC44$meth_per.x, GSC2_SC42_vs_GSC2_SC44$meth_per.y)
ggplot(GSC2_SC42_vs_GSC2_SC44, aes(x=meth_per.x, y=meth_per.y)) + geom_point() 

```






