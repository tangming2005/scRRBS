### use trimmomatic to trim adaptors and low quality bases

minimal read length `20 bp`. I did not trim any LEADING bases.

reads start with `GGG` are unmethylated.
reads start with `CCGG` are methylated.

```bash
java -jar /home/mtang1/tools/Trimmomatic-0.36/trimmomatic-0.36.jar SE -threads 4 GSC5-p_S48_L008_R1_001.fastq.gz GSC5-p_S48_trimmed_fastq.gz ILLUMINACLIP:/home/mtang1/tools/Trimmomatic-0.36/adapters/TruSeq_index_SE.fa:2:30:10  TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:20
```



### mapping with bowtie1

```bash
module load bowtie1
bowtie --chunkmbs 320 --best -p 10 /home/mtang1/ref_genomes/UCSC_hg19_ref/hg19 -q <(zcat GSC5-p_S48_trimmed_fastq.gz) -S GSC5-p_S48_trimmed.sam

```